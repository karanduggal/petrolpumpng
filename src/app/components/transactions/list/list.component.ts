import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { AlertService } from '../../../services/alert/alert.service';
import { iTransaction } from '../transaction.interface';
import { environment } from 'src/environments/environment';
import { DataTableDirective } from 'angular-datatables';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false }) transTable: DataTableDirective
  dtOptions: DataTables.Settings = { pagingType: 'full_numbers', order: [], pageLength: 5 };
  transactionsList: iTransaction[]; //INFO: For all transactions table.
  fetchedTableData: boolean = false; //INFO: For all transactions table.
  pendingTransactionsList: iTransaction[]; //INFO: For pending transactions table.
  fetchedPendingTableData: boolean = false; //INFO: For pending transactions table.
  hostUrl = environment.hostUrl
  customerId: string = ""
  totalPendingAmount: number = 0;

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.fetchQuery()
    this.getList();
    this.getPendingList();
  }

  //INFO: For all transactions
  getList() {
    this.apiService.callApi("get", `/transactions?customerId=${this.customerId}`, {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          this.transactionsList = data;
          this.rerender();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  getExcel() {
    this.apiService.callApi("get", `/transactions?getExcel=true&customerId=${this.customerId}`, {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          // this.transactionsList = data;
          this.downloadExcelFile(`${this.hostUrl}${data}`)
          // this.rerender();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  downloadExcelFile(path: string,) {
    const link = document.createElement('a');
    link.setAttribute('href', path);
    link.setAttribute('download', 'filename.xlsx');
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  //INFO: For pending transactions
  getPendingList() {
    this.apiService.callApi("get", `/transactions?type=pending&customerId=${this.customerId}`, {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          this.pendingTransactionsList = data;
          this.totalPendingAmount = this.pendingTransactionsList.reduce((acc, item) => {
            return acc + (item.amount - item.amountPaid)
          }, 0)
          this.rerenderPending();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  //INFO: For all transactions
  rerender() {
    const that = this;
    that.fetchedTableData = false;
    setTimeout(function () {
      that.fetchedTableData = true;
    }, 1);
  }

  //INFO: For pending transactions
  rerenderPending() {
    const that = this;
    that.fetchedPendingTableData = false;
    setTimeout(function () {
      that.fetchedPendingTableData = true;
    }, 1);
  }

  delete(transaction: iTransaction) {
    this.alertService.YesOrNo(`Delete`, `Are you sure you want to delete?`, '').then(approved => {
      if (!approved) return;
      this.apiService.callApi("delete", `/transactions/${transaction?._id}`, {}).subscribe({
        next: (response) => {
          this.transactionsList = this.transactionsList.filter((item) => item?._id?.toString() != transaction?._id);
          this.rerender();
        },
        error: (response) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      })
    })
  }

  fetchQuery() {
    this.activatedRoute.queryParams.subscribe({
      next: query => {
        const customerId = query['customerId']
        if (customerId) this.customerId = customerId
      }
    })
  }

}
