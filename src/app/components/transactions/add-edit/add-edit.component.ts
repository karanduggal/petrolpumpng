import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../services/alert/alert.service';
import { ApiService } from '../../../services/api/api.service';
import { iStaff } from '../../staff/staff.interface';
import { iBusiness } from '../../businesses/businesses.interface';
import { iProduct } from '../../products/product.interface';
import { iCustomer } from '../../customers/customer.interface';
import { iTransaction } from '../transaction.interface';
import * as moment from 'moment';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  addEditForm: FormGroup;
  currentForm: "Add" | "Edit" = "Add";
  submitted: boolean = false;
  renderedData: {
    staff: iStaff[],
    businesses: iBusiness[],
    products: iProduct[],
    customers: iCustomer[],
    transactionTypes: string[]
  }
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.selectView();
    this.renderFormData();
    this.buildForm();
    if (this.currentForm == "Edit") this.getTransaction();
  }

  selectView() {
    this.router.url.includes("add") ? this.currentForm = "Add" : this.currentForm = "Edit";
  }

  renderFormData() {
    this.apiService.callApi("get", "/transactions/render/form", {}).subscribe({
      next: (response) => {
        const { data } = response;
        this.renderedData = data;
      },
      error: (response) => {
        const { error } = response;
        this.alertService.alert(error.message, `error`);
      }
    })
  }

  buildForm() {
    this.addEditForm = new FormGroup({
      date: new FormControl("", [Validators.required]),
      _customer: new FormControl("", [Validators.required]),
      _staff: new FormControl("", [Validators.required]),
      _business: new FormControl("", [Validators.required]),
      _product: new FormControl("", [Validators.required]),
      quantity: new FormControl("", []), //required removed
      amount: new FormControl("", [Validators.required]),
      amountPaid: new FormControl("0", []),
      type: new FormControl("", [Validators.required]),
      receiptNo: new FormControl("", []), //required removed
      remarks: new FormControl("", []),
    }, 
    // { validators: this.checkPaidAmount }
    )
  }

  checkPaidAmount: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    const amount = group.get('amount');
    const amountPaid = group.get('amountPaid');
    if (amount?.valid && amountPaid?.valid) {
      if (amountPaid?.value > amount?.value) {
        amountPaid?.setErrors({ greaterThanActualAmount: true });
        return { greaterThanActualAmount: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  getTransactionId() {
    return new Promise((resolve, reject) => {
      this.route.params.subscribe((params: any) => {
        if (params.id) {
          resolve(params.id);
        } else {
          reject({ message: `Couldn't get transaction id.` })
        }
      })
    })
  }

  async getTransaction() {
    try {
      const transactionId = await this.getTransactionId();
      if (transactionId) {
        this.apiService.callApi("get", `/transactions/${transactionId}`, {}).subscribe({
          next: (response) => {
            const { data } = response;
            this.populateEditForm(data);
          },
          error: (response) => {
            const { error } = response;
            this.alertService.alert(error.message, `error`);
          }
        })
      }
    } catch (err: any) {
      console.log('getTransaction err:', err)
      this.alertService.alert(err.message, `error`);
    }
  }

  populateEditForm(transaction: iTransaction) {
    try {
      // console.log('transaction:', transaction)
      transaction.date && this.addEditForm.controls['date'].setValue(moment(transaction.date).format('YYYY-MM-DD'));
      transaction._staff && this.addEditForm.controls['_staff'].setValue(transaction._staff);
      transaction._business && this.addEditForm.controls['_business'].setValue(transaction._business);
      transaction._product && this.addEditForm.controls['_product'].setValue(transaction._product);
      transaction._customer && this.addEditForm.controls['_customer'].setValue(transaction._customer);
      transaction.quantity && this.addEditForm.controls['quantity'].setValue(transaction.quantity);
      transaction.amount && this.addEditForm.controls['amount'].setValue(transaction.amount);
      transaction.amountPaid && this.addEditForm.controls['amountPaid'].setValue(transaction.amountPaid);
      transaction.type && this.addEditForm.controls['type'].setValue(transaction.type);
      transaction.receiptNo && this.addEditForm.controls['receiptNo'].setValue(transaction.receiptNo);
      transaction.remarks && this.addEditForm.controls['remarks'].setValue(transaction.remarks);
    } catch (err: any) {
      console.log('populateEditForm err:', err);
      const { message } = err;
      this.alertService.alert(message || `Error populating form with data.`, `error`);
    }
  }

  get addEditFormControls() {
    return this.addEditForm.controls;
  }

  setProductAmount() {
    const _product = this.addEditFormControls['_product'].value;
    const quantity = this.addEditFormControls['quantity'].value || 0;
    const productPrice = this.renderedData.products.filter(p => p._id?.toString() == _product)[0].price;
    const amount = (quantity * (productPrice || 0)).toFixed(2);
    this.addEditFormControls['amount'].setValue(amount);
  }

  setProductQuantity() {
    const _product = this.addEditFormControls['_product'].value;
    const productPrice = this.renderedData.products.filter(p => p._id?.toString() == _product)[0].price;
    const amount = this.addEditFormControls['amount'].value;
    const quantity = (amount / (productPrice || 0)).toFixed(2);
    this.addEditFormControls['quantity'].setValue(quantity);
  }

  async submit() {
    this.submitted = true;
    const amountPaid =  this.addEditForm.controls['amountPaid'].value
    if(typeof(amountPaid) === 'string' || !amountPaid) this.addEditForm.controls['amountPaid'].setValue(0)
  
    if (this.addEditForm.valid) {
      if (this.currentForm === "Add") {
        this.apiService.callApi("post", "/transactions", this.addEditForm.value).subscribe({
          next: ((response) => {
            const { message } = response;
            this.alertService.alert(message, 'success');
            this.router.navigate(['/transactions/list']);
          }),
          error: ((response: any) => {
            const { error } = response;
            this.alertService.alert(error.message, `error`);
          })
        })
      } else {
        const transactionId = await this.getTransactionId();
        this.apiService.callApi("patch", `/transactions/${transactionId}`, this.addEditForm.value).subscribe({
          next: ((response) => {
            const { message } = response;
            this.alertService.alert(message, 'success');
            this.router.navigate(['/transactions/list']);
          }),
          error: ((response: any) => {
            const { error } = response;
            this.alertService.alert(error.message, `error`);
          })
        })
      }
    }
  }

}
