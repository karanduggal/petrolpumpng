import { iStaff } from '../staff/staff.interface';
import { iBusiness } from '../businesses/businesses.interface';
import { iProduct } from '../products/product.interface';
import { iCustomer } from '../customers/customer.interface';
export interface iTransaction {
    _id?: string
    date?: string,
    _customer?: string,
    customer?: iCustomer,
    _staff?: string,
    staff?: iStaff,
    _business?: string,
    business?: iBusiness,
    _product?: string,
    product?: iProduct,
    quantity?: number,
    unit?: string,
    amount: number,
    amountPaid: number,
    type?: "Cash" | "Credit/Debit" | "Bank-Transfer" | "Other",
    receiptNo?: string,
    remarks?: string
}