import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StaffRoutingModule } from './staff-routing.module';
import { AddEditComponent } from './add-edit/add-edit.component';
import { ListComponent } from './list/list.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [
    AddEditComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    StaffRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class StaffModule { }
