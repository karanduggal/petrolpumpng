export interface iStaff {
    _id?: string,
    name?: string,
    doj?: string,
    contactNumber?: string,
    email?: string,
    password?: string,
    role?: "Accountant" | "Pump-Executive" | "Others"
}