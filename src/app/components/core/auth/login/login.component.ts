import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from '../../../../services/alert/alert.service';
import { ApiService } from '../../../../services/api/api.service';
import { SharedService } from '../../../../services/shared/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginFrom: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private alertService: AlertService,
    private router: Router,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.verifyAlreadyLoggedInUser();
    this.form();
  }
  async verifyAlreadyLoggedInUser() {
    if (await this.sharedService.verifyLogin()) {
      this.router.navigate(['/dashboard'])
    }
  }
  form() {
    this.loginFrom = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required,]),
      role: new FormControl('Admin')
    })
  }

  submit() {
    if (this.loginFrom.valid) {
      this.apiService.callApi('post', '/auth', this.loginFrom.value).subscribe(result => {
        const { data, token } = result;
        sessionStorage.setItem('token', token);
        this.alertService.alert(`Welcome Admin!`, `success`)
        this.router.navigate(['/dashboard']);
      }, err => {
        if (sessionStorage.getItem('token')) sessionStorage.removeItem('token');
        const { error } = err;
        if (error && error.message) this.alertService.alert(`${error.message}`, `error`)
        else this.alertService.alert(`Technical issue!`, `error`)
      })
    }
  }
}
