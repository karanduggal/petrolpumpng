import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SharedService } from '../../../services/shared/shared.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  shownavbar:boolean = false;
  showNav: boolean = false;
  blockedRoutes: string[] = ['/', '/auth/login'];
  constructor(
    private router: Router,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.checkCurrentRoute();
  }

  checkCurrentRoute() {
    this.router.events.subscribe(async event => {
      if (event instanceof NavigationEnd) {
        event = event as NavigationEnd;
        if (!this.blockedRoutes.includes(event.url) && await this.sharedService.verifyLogin()) {
          this.showNav = true;
        } else {
          this.showNav = false;
        }
      }
    })
  }

  logout() {
    this.sharedService.logout();
  }
  toggle(){
    this.shownavbar = !this.shownavbar;
  }
}
