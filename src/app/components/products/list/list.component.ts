import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../services/shared/shared.service';
import { iProduct } from '../product.interface';
import { ApiService } from '../../../services/api/api.service';
import { AlertService } from '../../../services/alert/alert.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  dtOptions: DataTables.Settings = { pagingType: 'full_numbers', order: [], pageLength: 5 };
  productsList: iProduct[];
  fetchedTableData: boolean = false;

  constructor(
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.apiService.callApi("get", "/products", {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          this.productsList = data;
          this.rerender();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  rerender() {
    const that = this;
    that.fetchedTableData = false;
    setTimeout(function () {
      that.fetchedTableData = true;
    }, 1);
  }

  delete(product: iProduct) {
    this.alertService.YesOrNo(`Delete`, `Are you sure you want to delete?`, '').then(approved => {
      if (!approved) return;
      this.apiService.callApi("delete", `/products/${product?._id}`, {}).subscribe({
        next: (response) => {
          this.productsList = this.productsList.filter((item) => item?._id?.toString() != product?._id);
          this.rerender();
        },
        error: (response) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      })
    })
  }

  resetPrice(input: any, price: number) {
    input.disabled = true;
    input.value = price;
  }

  changeProductPrice(product: iProduct, price: number) {
    price = parseFloat(price.toFixed(2));
    product.price = price;
    this.apiService.callApi("get", `/products/${product._id?.toString()}/${price}`, {}).subscribe(
      {
        next: (response) => {
          this.alertService.alert(response.message, `success`);
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  changeStringToNumber(num: any) {
    return parseFloat(num);
  }

}
