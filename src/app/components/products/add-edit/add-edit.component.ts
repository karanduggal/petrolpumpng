import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../services/alert/alert.service';
import { iProduct } from '../product.interface';
import { ApiService } from '../../../services/api/api.service';
@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  addEditForm: FormGroup;
  currentForm: "Add" | "Edit" = "Add";
  submitted: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.selectView();
    this.buildForm();
    if (this.currentForm == "Edit") this.getBusiness();
  }

  selectView() {
    this.router.url.includes("add") ? this.currentForm = "Add" : this.currentForm = "Edit";
  }

  buildForm() {
    this.addEditForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      unit: new FormControl("", [Validators.required]),
      remarks: new FormControl("", []),
    })
  }

  getBusiness() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        //TODO: Make an API call to fetch business detail by ID and populate form with details afterwards.
        this.populateEditForm({} as iProduct);
      }
    })
  }

  populateEditForm(data: iProduct) {
    this.addEditForm = new FormGroup({
      name: new FormControl("Sample", [Validators.required]),
      contactNumber: new FormControl("9876543210", [Validators.required]),
      contactEmail: new FormControl("s@gmail.com", [Validators.required]),
      password: new FormControl("test@123", [Validators.required]),
      confirmPassword: new FormControl("test@123", [Validators.required]),
      country: new FormControl("India", []),
      state: new FormControl("Punjab", []),
      city: new FormControl("Mohali", []),
      district: new FormControl("Mohali", []),
      address: new FormControl("999", []),
      remarks: new FormControl("Sample", []),
      logo: new FormControl("", []),
    })
  }

  get addEditFormControls() {
    return this.addEditForm.controls;
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    const passwordField = group.get('password');
    const confirmPasswordControl = group.get('confirmPassword');
    if (passwordField?.valid && confirmPasswordControl?.valid) {
      if (passwordField?.value !== confirmPasswordControl?.value) {
        confirmPasswordControl?.setErrors({ unmatched: true });
        return { unmatched: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  submit() {
    this.submitted = true;
    if (this.addEditForm.valid) {
      delete this.addEditForm.value.confirmPassword;
      this.apiService.callApi("post", "/products", this.addEditForm.value).subscribe({
        next: ((response) => {
          const { message } = response;
          this.alertService.alert(message, 'success');
          this.router.navigate(['/products/list']);
        }),
        error: ((response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        })
      })
    }
  }

}
