export interface iProduct {
    _id?: string,
    name?: string,
    unit?: "KG" | "GRAMS" | "PIECE" | "LITRE" | "ML",
    remarks?: string,
    price?: number
}