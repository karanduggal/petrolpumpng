import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { AlertService } from '../../services/alert/alert.service';
import { iTransaction } from '../transactions/transaction.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dtOptions: DataTables.Settings = { pagingType: 'full_numbers', order: [], pageLength: 10 };
  totalBusinesses: number = 0
  totalTransactions: number = 0
  totalPaymentCollected: number = 0
  totalPendingPayment: number = 0
  majorTransacionsThisMonth: iTransaction[] = [];
  fetchedMajorTransactionThisMonthData: boolean = false;
  constructor(
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.getTotalsData()
    this.getTopMajorTransactionsInCurrentMonth();
  }

  getTotalsData() {
    this.apiService.callApi("get", '/reports', {}).subscribe({
      next: (response) => {
        const { data } = response;
        const { totalBusinesses, totalTransactions, totalPaymentCollected, totalPendingPayment } = data;
        this.totalBusinesses = totalBusinesses;
        this.totalTransactions = totalTransactions;
        this.totalPaymentCollected = totalPaymentCollected;
        this.totalPendingPayment = totalPendingPayment;
      },
      error: (response) => {
        const { error } = response;
        this.alertService.alert(error.message, `error`);
      }
    })
  }

  getTopMajorTransactionsInCurrentMonth() {
    this.apiService.callApi("post", '/transactions/custom?type=top-10-major-this-month&limit=10', {}).subscribe({
      next: (response) => {
        const { data } = response;
        this.majorTransacionsThisMonth = data;
        this.rerenderTopMajorTransactionsInCurrentMonthTable();
      },
      error: (response) => {
        const { error } = response;
        this.alertService.alert(error.message, `error`);
      }
    })
  }

  rerenderTopMajorTransactionsInCurrentMonthTable() {
    const that = this;
    that.fetchedMajorTransactionThisMonthData = false;
    setTimeout(function () {
      that.fetchedMajorTransactionThisMonthData = true;
    }, 1);
  }

  deleteTopMajorTransactionsInCurrentMonthTable(transaction: iTransaction) {
    this.alertService.YesOrNo(`Delete`, `Are you sure you want to delete?`, '').then(approved => {
      if (!approved) return;
      this.apiService.callApi("delete", `/transactions/${transaction?._id}`, {}).subscribe({
        next: (response) => {
          this.majorTransacionsThisMonth = this.majorTransacionsThisMonth.filter((item) => item?._id?.toString() != transaction?._id);
          this.rerenderTopMajorTransactionsInCurrentMonthTable();
        },
        error: (response) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      })
    })
  }

}
