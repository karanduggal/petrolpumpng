export interface iBusiness {
    _id?: string,
    name?: string,
    contactNumber?: string,
    email?: string,
    password?: string,
    country?: string,
    state?: string,
    city?: string,
    address?: string,
    remarks?: string,
    district?: string,
    logo?: string
}