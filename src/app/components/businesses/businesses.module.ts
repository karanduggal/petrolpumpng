import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BusinessesRoutingModule } from './businesses-routing.module';
import { AddEditComponent } from './add-edit/add-edit.component';
import { ListComponent } from './list/list.component';
import { DataTablesModule } from 'angular-datatables';
import { ViewComponent } from './view/view.component';


@NgModule({
  declarations: [
    AddEditComponent,
    ListComponent,
    ViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BusinessesRoutingModule,
    DataTablesModule
  ]
})
export class BusinessesModule { }
