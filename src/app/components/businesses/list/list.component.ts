import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../services/shared/shared.service';
import { iBusiness } from '../businesses.interface';
import { ApiService } from '../../../services/api/api.service';
import { AlertService } from '../../../services/alert/alert.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  dtOptions: DataTables.Settings = { pagingType: 'full_numbers', order: [], pageLength: 5 };
  businessesList: iBusiness[];
  fetchedTableData: boolean = false;

  constructor(
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.apiService.callApi("get", "/businesses", {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          this.businessesList = data;
          this.rerender();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  rerender() {
    const that = this;
    that.fetchedTableData = false;
    setTimeout(function () {
      that.fetchedTableData = true;
    }, 1);
  }

  delete(business: iBusiness) {
    this.alertService.YesOrNo(`Delete`, `Are you sure you want to delete?`, '').then(approved => {
      if (!approved) return;
      this.apiService.callApi("delete", `/businesses/${business?._id}`, {}).subscribe({
        next: (response) => {
          this.businessesList = this.businessesList.filter((item) => item?._id?.toString() != business?._id);
          this.rerender();
        },
        error: (response) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      })
    })
  }

}
