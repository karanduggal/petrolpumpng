import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../services/alert/alert.service';
import { iCustomer } from '../customer.interface';
import { ApiService } from '../../../services/api/api.service';
import { iBusiness } from '../../businesses/businesses.interface';
@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  addEditForm: FormGroup;
  currentForm: "Add" | "Edit" = "Add";
  submitted: boolean = false;
  businesses :  iBusiness[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.selectView();
    this.buildForm();
    this.getBusinesses();
    if (this.currentForm == "Edit") this.getBusiness();
  }

  selectView() {
    this.router.url.includes("add") ? this.currentForm = "Add" : this.currentForm = "Edit";
  }

  buildForm() {
    this.addEditForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      contactNumber: new FormControl("", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]),
      role: new FormControl("Customer", [Validators.required]),
      _business: new FormControl("", [Validators.required]),
    })
  }

  getBusiness() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        //TODO: Make an API call to fetch business detail by ID and populate form with details afterwards.
        this.populateEditForm({} as iCustomer);
      }
    })
  }
  getBusinesses() {
    this.apiService.callApi("get", "/businesses", {}).subscribe({
      next: (response) => {
        const { data } = response;
        this.businesses = data;
      },
      error: (response) => {
        const { error } = response;
        this.alertService.alert(error.message, `error`);
      }
    })
  }
  populateEditForm(data: iCustomer) {

  }

  get addEditFormControls() {
    return this.addEditForm.controls;
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    const passwordField = group.get('password');
    const confirmPasswordControl = group.get('confirmPassword');
    if (passwordField?.valid && confirmPasswordControl?.valid) {
      if (passwordField?.value !== confirmPasswordControl?.value) {
        confirmPasswordControl?.setErrors({ unmatched: true });
        return { unmatched: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  submit() {
    this.submitted = true;
    if (this.addEditForm.valid) {
      delete this.addEditForm.value.confirmPassword;
      this.apiService.callApi("post", "/users", this.addEditForm.value).subscribe({
        next: ((response) => {
          const { message } = response;
          this.alertService.alert(message, 'success');
          this.router.navigate(['/customers/list']);
        }),
        error: ((response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        })
      })
    }
  }

}
