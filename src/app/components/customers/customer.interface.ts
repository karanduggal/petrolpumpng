export interface iCustomer {
    _id?: string,
    name?: string,
    contactNumber?: string,
    email?: string,
    pendingAmount?: number;
    role?: "Customer"
}