import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api/api.service';
import { AlertService } from '../../../services/alert/alert.service';
import { iCustomer } from '../customer.interface';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  dtOptions: DataTables.Settings = { pagingType: 'full_numbers', order: [], pageLength: 5 };
  staffList: iCustomer[];
  fetchedTableData: boolean = false;

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.apiService.callApi("get", "/users/customers/list", {}).subscribe(
      {
        next: (response) => {
          const { data } = response;
          this.staffList = data;
          this.rerender();
        },
        error: (response: any) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      }
    )
  }

  rerender() {
    const that = this;
    that.fetchedTableData = false;
    setTimeout(function () {
      that.fetchedTableData = true;
    }, 1);
  }

  delete(staff: iCustomer) {
    this.alertService.YesOrNo(`Delete`, `Are you sure you want to delete?`, '').then(approved => {
      if (!approved) return;
      this.apiService.callApi("delete", `/users/${staff?._id}`, {}).subscribe({
        next: (response) => {
          this.staffList = this.staffList.filter((item) => item?._id?.toString() != staff?._id);
          this.rerender();
        },
        error: (response) => {
          const { error } = response;
          this.alertService.alert(error.message, `error`);
        }
      })
    })
  }

  userTranscations(staff: iCustomer){
      this.router.navigate(['/transactions/list'],{
        queryParams: {
          customerId: staff._id
        }
      })
  }

}
