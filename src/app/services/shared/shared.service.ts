import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  //TODO: Just for preview purposes. Remove after API integration.
  public businesses = new BehaviorSubject([
    { name: 'Business 1', contactNumber: '8124686421', contactEmail: 'b1@gmail.com', state: 'Punjab', city: 'Mohali', district: 'SAS' },
    { name: 'Business 2', contactNumber: '9724686421', contactEmail: 'b2@gmail.com', state: 'Himachal', city: 'Moga', district: 'HAS' },
    { name: 'Business 3', contactNumber: '6768857857', contactEmail: 'b3@gmail.com', state: 'Gujrat', city: 'Srinagar', district: 'MAS' },
    { name: 'Business 4', contactNumber: '3887584588', contactEmail: 'b4@gmail.com', state: 'Mumbai', city: 'Chandigarh', district: 'HAS' },
    { name: 'Business 5', contactNumber: '7637676776', contactEmail: 'b5@gmail.com', state: 'Mumbai', city: 'Mohali', district: 'HAS' },
    { name: 'Business 6', contactNumber: '3676767673', contactEmail: 'b6@gmail.com', state: 'Punjab', city: 'Moga', district: 'HAS' },
    { name: 'Business 7', contactNumber: '7367376777', contactEmail: 'b7@gmail.com', state: 'Himachal', city: 'Chandigarh', district: 'HAS' },
    { name: 'Business 8', contactNumber: '3676767676', contactEmail: 'b8@gmail.com', state: 'Gujrat', city: 'Moga', district: 'HAS' },
    { name: 'Business 9', contactNumber: '3676767632', contactEmail: 'b9@gmail.com', state: 'Himachal', city: 'Mohali', district: 'MAS' },
    { name: 'Business 10', contactNumber: '9784978689', contactEmail: 'b10@gmail.com', state: 'Mumbai', city: 'Chandigarh', district: 'HAS' },
    { name: 'Business 11', contactNumber: '9686478888', contactEmail: 'b11@gmail.com', state: 'Gujrat', city: 'Srinagar', district: 'HAS' },
    { name: 'Business 12', contactNumber: '3678687887', contactEmail: 'b12@gmail.com', state: 'Himachal', city: 'Moga', district: 'MAS' },
    { name: 'Business 13', contactNumber: '9736786888', contactEmail: 'b13@gmail.com', state: 'Punjab', city: 'Srinagar', district: 'MAS' },
  ]);
  constructor(
    private router: Router,
    private apiService: ApiService
  ) { }

  verifyLogin(): Promise<boolean> | boolean {
    return new Promise(resolve => {
      if (!sessionStorage.getItem('token')) return resolve(false);
      this.apiService.callApi("get", "/auth", {}).subscribe(
        (data) => {
          // console.log('canActivate data:', data)
          return resolve(true)
        },
        (error) => {
          this.logout();
          return resolve(false);
        }
      );
    });
  }

  logout() {
    if (sessionStorage.getItem('token')) sessionStorage.removeItem('token');
    this.router.navigate(['/auth/login']);
  }
}
