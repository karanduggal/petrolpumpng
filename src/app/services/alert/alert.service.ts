import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }
  alert(Title: string, Icon: any) {
    Swal.fire({
      position: 'top',
      icon: Icon,
      title: Title,
      showConfirmButton: false,
      timer: 1500,
      toast: true
    })
  }
  async YesOrNo(Title: string, Text: string, Icon: any) {
    const result = await Swal.fire({
      title: Title,
      text: Text,
      icon: Icon,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    });
    if (result.isConfirmed) {
      return true;
    }
    return false;
  }
}
