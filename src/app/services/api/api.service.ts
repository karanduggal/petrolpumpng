import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Observer } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseURL = environment.baseURL

  constructor(
    private http: HttpClient
  ) { }
  callAPI(method: String, apidata: any, APIEndPoint: String) {
    if (method === 'post') {
      return this.http.post(this.baseURL + APIEndPoint, apidata);
    } else if (method === 'get') {
      return this.http.get(this.baseURL + APIEndPoint, apidata);
    } else if (method === 'delete') {
      return this.http.delete(this.baseURL + APIEndPoint, apidata);
    } else if (method === 'put') {
      return this.http.put(this.baseURL + APIEndPoint, apidata);
    }
    else {
      return this.http.get(this.baseURL + APIEndPoint, apidata);
    }
  }

  //TODO: Make an error and header interceptors for api requests calls.
  callApi(method: "get" | "post" | "put" | "patch" | "delete", route: string, data: any): Observable<any> {
    return new Observable((observer: Observer<any>) => {
      this.http[method](this.baseURL + route, data)
        .subscribe((data: any) => {
          observer.next(data);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }
}
