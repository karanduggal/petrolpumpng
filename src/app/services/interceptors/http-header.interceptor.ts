import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from "@angular/common/http";
import { Observable } from 'rxjs';
export class HeaderInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token: string = ``;

    if (sessionStorage.getItem("token")) token = sessionStorage.getItem("token") as string;

    const clonedRequest = req.clone({
      headers: req.headers
        .set('Authorization', token ? `Bearer ${token}` : '')
        .set('x-api-key', `petrolpumpgsbitlabs`),
    });

    return next.handle(clonedRequest);
  }
}
