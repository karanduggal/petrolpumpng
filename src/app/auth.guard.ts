import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { SharedService } from './services/shared/shared.service';

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(
    private sharedService: SharedService,
    private router: Router
  ) { }

  canActivate(): Promise<boolean> | boolean {
    return new Promise(async resolve => {
      if (await this.sharedService.verifyLogin()) {
        resolve(true);
      } else {
        this.router.navigate(['/auth/login'])
      }
    })
  }


}