import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path: "",
    redirectTo: "auth",
    pathMatch: "full"
  },
  {
    path: 'auth',
    loadChildren: () => import('./components/core/auth/auth.module').then(mod => mod.AuthModule)
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/dashboard/dashboard.module').then(mod => mod.DashboardModule),
  },
  {
    path: 'businesses',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/businesses/businesses.module').then(mod => mod.BusinessesModule)
  },
  {
    path: 'transactions',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/transactions/transactions.module').then(mod => mod.TransactionsModule),
  },
  {
    path: 'products',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/products/products.module').then(mod => mod.ProductsModule)
  },
  {
    path: 'staff',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/staff/staff.module').then(mod => mod.StaffModule)
  },
  {
    path: 'customers',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/customers/customers.module').then(mod => mod.CustomersModule)
  },
  { path: '**', redirectTo: '/auth/login' } //TODO: Redirect unknown routes somewhere.
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
